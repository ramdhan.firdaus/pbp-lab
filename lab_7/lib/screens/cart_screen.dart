import 'package:flutter/material.dart';
import '../utils/ui_helper.dart';
import '../widgets/custom_divider_view.dart';
import '../widgets/veg_badge_view.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Barang Belanjaan'),
        backgroundColor: Colors.black87,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.only(top: 15.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _OrderView(),
                MyCustomForm(),
                CustomDividerView(dividerHeight: 15.0),
                CustomDividerView(dividerHeight: 15.0),
                _BillDetailView(),
                _DecoratedView(),
                _AddressPaymentView(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _OrderView extends StatefulWidget {
  @override
  _OrderViewState createState() => _OrderViewState();
}

class _OrderViewState extends State<_OrderView> {
  int cartCount1 = 1;
  int cartCount2 = 1;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Image.asset(
                'assets/images/masker10.jpg',
                height: 60.0,
                width: 60.0,
              ),
              UIHelper.horizontalSpaceSmall(),
              Column(
                children: <Widget>[
                  Text('Masker N99', style: Theme.of(context).textTheme.subtitle2),
                  UIHelper.verticalSpaceExtraSmall(),
                  Text('RP28000', style: Theme.of(context).textTheme.subtitle2)
                ],
              )
            ],
          ),
          UIHelper.verticalSpaceLarge(),
          Row(
            children: <Widget>[
              VegBadgeView(),
              UIHelper.horizontalSpaceSmall(),
              Text(
                'Deskripsi Masker N99',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Spacer(),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                height: 35.0,
                width: 100.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                      child: Icon(Icons.remove, color: Colors.green),
                      onTap: () {
                        if (cartCount1 > 0) {
                          setState(() {
                            cartCount1 -= 1;
                          });
                        }
                      },
                    ),
                    Spacer(),
                    Text('$cartCount1', style: Theme.of(context).textTheme.subtitle2!.copyWith(fontSize: 16.0)),
                    Spacer(),
                    InkWell(
                      child: Icon(Icons.add, color: Colors.green),
                      onTap: () {
                        setState(() {
                          cartCount1 += 1;
                        });
                      },
                    )
                  ],
                ),
              ),
              UIHelper.horizontalSpaceSmall(),
            ],
          ),
          UIHelper.verticalSpaceLarge(),
          Row(
            children: <Widget>[
              new Image.asset(
                'assets/images/masker2.jpg',
                height: 60.0,
                width: 60.0,
              ),
              UIHelper.horizontalSpaceSmall(),
              Column(
                children: <Widget>[
                  Text('Masker Bedah Hitam', style: Theme.of(context).textTheme.subtitle2),
                  UIHelper.verticalSpaceExtraSmall(),
                  Text('RP3000', style: Theme.of(context).textTheme.subtitle2)
                ],
              )
            ],
          ),
          UIHelper.verticalSpaceLarge(),
          Row(
            children: <Widget>[
              VegBadgeView(),
              UIHelper.horizontalSpaceSmall(),
              Text(
                'Deskripsi Masker Bedah Hitam',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Spacer(),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 5.0),
                height: 35.0,
                width: 100.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                      child: Icon(Icons.remove, color: Colors.green),
                      onTap: () {
                        if (cartCount2 > 0) {
                          setState(() {
                            cartCount2 -= 1;
                          });
                        }
                      },
                    ),
                    Spacer(),
                    Text('$cartCount2', style: Theme.of(context).textTheme.subtitle2!.copyWith(fontSize: 16.0)),
                    Spacer(),
                    InkWell(
                      child: Icon(Icons.add, color: Colors.green),
                      onTap: () {
                        setState(() {
                          cartCount2 += 1;
                        });
                      },
                    )
                  ],
                ),
              ),
              UIHelper.horizontalSpaceSmall(),
            ],
          ),
          UIHelper.verticalSpaceLarge(),
        ],
      ),
    );
  }
}

class _BillDetailView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 16.0);

    return Container(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Ringkasan',
            style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 17.0),
          ),
          UIHelper.verticalSpaceMedium(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Jumlah Item', style: textStyle),
              Text('2', style: textStyle),
            ],
          ),
          UIHelper.verticalSpaceMedium(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text('Total Harga', style: textStyle),
              Text('RP31000', style: textStyle),
            ],
          ),
          UIHelper.verticalSpaceMedium(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Catatan', style: textStyle),
                        UIHelper.horizontalSpaceSmall(),
                        Icon(Icons.info_outline, size: 14.0)
                      ],
                    ),
                  ],
                ),
              ),
              Text('Ini adalah sebuah catatan', style: textStyle),
            ],
          ),
          UIHelper.verticalSpaceLarge(),
          _buildDivider(),
          Container(
            alignment: Alignment.center,
            height: 60.0,
            child: Row(
              children: <Widget>[
                Text('Total Pembayaran', style: Theme.of(context).textTheme.subtitle2),
                Spacer(),
                Text('RP31000', style: textStyle),
              ],
            ),
          ),
        ],
      ),
    );
  }

  CustomDividerView _buildDivider() => CustomDividerView(
        dividerHeight: 1.0,
        color: Colors.grey[400],
      );
}

class _DecoratedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      color: Colors.grey[200],
    );
  }
}

class _AddressPaymentView extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.all(10.0),
                height: 58.0,
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Belanjaan diproses')),
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.green, // background
                    onPrimary: Colors.blue, // foreground
                  ),
                  child: Text(
                      'Checkout',
                      style: Theme.of(context).textTheme.subtitle2!.copyWith(color: Colors.white),
                  ),

                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB( 20, 0, 20, 0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Catatan',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Catatan dimasukan')),
                  );
                }
              },
              child: const Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}