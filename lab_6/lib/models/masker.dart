class Masker {
  final String image;
  final String name;
  final String desc;
  final String ratingPrice;

  const Masker({
    required this.image,
    required this.name,
    required this.desc,
    required this.ratingPrice,
  });

  static List<Masker> getMasker() {
    return [
      Masker(
        image: 'assets/images/masker1.jpg',
        name: 'Masker Bedah Biru',
        desc: 'Deskripsi Masker Bedah Biru',
        ratingPrice: '4.1 - Rp2500',
      ),
      Masker(
        image: 'assets/images/masker2.jpg',
        name: 'Masker Bedah Hitam',
        desc: 'Deskripsi Masker Bedah Hitam',
        ratingPrice: '4.3 - Rp3000',
      ),
      Masker(
        image: 'assets/images/masker3.jpg',
        name: 'Masker Scuba',
        desc: 'Deskripsi Masker Scuba',
        ratingPrice: '4.1 - Rp3500',
      ),
      Masker(
        image: 'assets/images/masker4.jpg',
        name: 'Masker Kain Polos',
        desc: 'Deskripsi Masker Kain Polos',
        ratingPrice: '4.3 - Rp4000',
      ),
      Masker(
        image: 'assets/images/masker5.jpg',
        name: 'Masker Kain Vista',
        desc: 'Deskripsi Masker Kain Vista',
        ratingPrice: '4.2 - Rp5000',
      ),
      Masker(
        image: 'assets/images/masker6.jpg',
        name: 'Masker N95',
        desc: 'Deskripsi Masker N95',
        ratingPrice: '3.8 - Rp6000',
      ),
      Masker(
        image: 'assets/images/masker7.jpg',
        name: 'Masker KN95',
        desc: 'Deskripsi Masker KN95',
        ratingPrice: '3.8 - Rp7000',
      ),
      Masker(
        image: 'assets/images/masker8.jpg',
        name: 'Masker FFP3',
        desc: 'Deskripsi Masker FFP3',
        ratingPrice: '3.8 - Rp15000',
      ),
      Masker(
        image: 'assets/images/masker9.jpg',
        name: 'Masker Rider',
        desc: 'Deskripsi Masker Rider',
        ratingPrice: '3.8 - Rp25000',
      ),
      Masker(
        image: 'assets/images/masker10.jpg',
        name: 'Masker N99',
        desc: 'Deskripsi Masker N99',
        ratingPrice: '3.8 - Rp28000',
      ),
    ];
  }
}
