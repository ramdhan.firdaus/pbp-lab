# Generated by Django 3.1.5 on 2021-09-15 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Friend',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('npm', models.CharField(max_length=10)),
                ('dob', models.DateField()),
            ],
        ),
    ]
