import 'package:flutter/material.dart';
import 'package:pbp_c07/screens/home_bottom_navigation_screen.dart';

import 'shared/app_theme.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PBP-C07',
      debugShowCheckedModeBanner: false,
      theme: appPrimaryTheme(),
      home: HomeBottomNavigationScreen(),
    );
  }
}
