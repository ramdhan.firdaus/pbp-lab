import 'package:flutter/material.dart';

class AllKeuntungan {
  final String motto;
  final String penjelasan;

  const AllKeuntungan({
    required this.motto,
    required this.penjelasan,
  });
}
