import 'package:flutter/material.dart';
import 'package:pbp_c07/dummy_data.dart';
import 'package:pbp_c07/utils/ui_helper.dart';

class TopPicksForYouView extends StatelessWidget {
  final masker = DUMMY_TOPMASKER;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Icon(Icons.masks_rounded, size: 20.0),
              UIHelper.horizontalSpaceSmall(),
              Text(
                'Our Masker',
                style: Theme.of(context).textTheme.headline4!.copyWith(fontSize: 20.0),
              )
            ],
          ),
          UIHelper.verticalSpaceLarge(),
          LimitedBox(
            maxHeight: 188.0,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: masker.length,
              itemBuilder: (context, index) => InkWell(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  width: 100.0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                            )
                          ],
                        ),
                        child: Image.asset(
                          masker[index].image,
                          width: 100.0,
                          height: 100.0,
                          fit: BoxFit.cover,
                        ),
                      ),
                      UIHelper.verticalSpaceSmall(),
                      Flexible(
                        child: Text(
                          masker[index].name,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.subtitle2!.copyWith(
                                fontSize: 14.0,
                                fontWeight: FontWeight.w600,
                              ),
                        ),
                      ),
                      UIHelper.verticalSpaceExtraSmall(),
                      Text(
                        masker[index].price,
                        maxLines: 1,
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: Colors.grey[700],
                              fontSize: 13.0,
                            ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
