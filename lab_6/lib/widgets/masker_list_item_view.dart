import 'package:flutter/material.dart';
import 'package:pbp_c07/models/masker.dart';
import 'package:pbp_c07/utils/ui_helper.dart';

class MaskerListItemView extends StatelessWidget {
  const MaskerListItemView({
    Key? key,
    required this.masker,
  }) : super(key: key);

  final Masker masker;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 2.0,
                )
              ],
            ),
            child: Image.asset(
              masker.image,
              height: 80.0,
              width: 80.0,
              fit: BoxFit.fill,
            ),
          ),
          UIHelper.horizontalSpaceSmall(),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  masker.name,
                  style: Theme.of(context).textTheme.subtitle2!.copyWith(fontSize: 15.0),
                ),
                Text(masker.desc,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(color: Colors.grey[600], fontSize: 13.5)),
                UIHelper.verticalSpaceSmall(),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.star,
                      size: 14.0,
                      color: Colors.grey[600],
                    ),
                    Text(masker.ratingPrice)
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: ElevatedButton(
              style: TextButton.styleFrom(
                  backgroundColor: Color(0xff38ce24)),
              onPressed: () {
              },
              child: const Text('Add'),
            ),
          ),
        ],
      ),
    );
  }
}
