import 'package:flutter/material.dart';
import 'package:pbp_c07/screens/customize_screen.dart';
import 'package:pbp_c07/screens/home_screen.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 180,
            width: double.infinity,
            padding: EdgeInsets.all(40),
            alignment: Alignment.centerLeft,
            color: Colors.black54,
            child: Center( child: Text(
              'C07 MASKER',
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Colors.white)
            ),
            ),
          ),
          ListTile(
            title: const Text('Home'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => new Screen()),
              );
            },
          ),
          ListTile(
            title: const Text('Customize'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => new CustomizeScreen()),
              );
            },
          ),
        ],
      ),
    );

  }
}