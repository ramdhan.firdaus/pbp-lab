class TopMasker {
  final String image;
  final String name;
  final String price;

  const TopMasker({
    required this.image,
    required this.name,
    required this.price,
  });
}

