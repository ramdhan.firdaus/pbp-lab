import 'package:flutter/material.dart';
import 'package:pbp_c07/utils/ui_helper.dart';
import 'package:pbp_c07/widgets/custom_divider_view.dart';

class CustomizeScreen extends StatelessWidget {
  static const routeName = '/about';
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    final judul = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Design Your Own Mask!',
        style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    );

    final designJudul = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Design Your Own Mask!',
        style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold, color: Colors.black),
      ),
    );


    final lorem = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('\nMasker bedah atau bisa disebut sebagai masker medis yang biasanya berwarna hijau atau biru. '
          'Masker jenis ini mampu menahan droplet sekitar 80-90 persen. Masker ini hanya bisa digunakan satu kali pakai dalam waktu 4 jam pemakaian. '
          'asker ini terutama wajib digunakan oleh pasien sakit dan petugas kesehatan yang tidak menangani pasien COVID-19 secara langsung. '
          'Petugas yang menangani pasien COVID-19 secara langsung wajib mengenakan masker N-95 dan APD level 3',
        textAlign: TextAlign.justify,
        style: TextStyle(fontSize: 20.0, color: Colors.black),
      ),
    );

    final pengumuman = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('\nPERHATIAN: Item ini tidak dapat dibatalkan kecuali menghubungi sosial media kami',
        textAlign: TextAlign.justify,
        style: TextStyle(fontSize: 20.0, color: Colors.red),
      ),
    );

    final harga = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('Harga: Rp15000',
        textAlign: TextAlign.justify,
      ),
    );

    final minimal = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('Minimal pemesanan: 100 Unit',
        textAlign: TextAlign.justify,
      ),
    );


    final body = Container(
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.grey[200]
      ),
      child: Column(
        children: <Widget>[
          judul,
          CustomDividerView(),
          new Image.asset(
            'assets/images/bedah.jpg',
            width: 400.0,
            height: 240.0,
          ),
          CustomDividerView(),
          CustomDividerView(),
          harga,
          minimal,
          CustomDividerView(),
          CustomDividerView(),
          designJudul,
          CustomDividerView(),
          MyStatefulWidget(),
          MyStatefulWidget2(),
          MyStatefulWidget3(),
          MyStatefulWidget4(),
          CustomDividerView(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ElevatedButton(
                style: TextButton.styleFrom(
                    backgroundColor: Color(0xff38ce24)),
                onPressed: () {
                },
                child: const Text('Add To Cart'),
              ),
              ElevatedButton(
                style: TextButton.styleFrom(
                    backgroundColor: Color(0xff38ce24)),
                onPressed: () {
                },
                child: const Text('Add To Wishlist'),
              ),
            ],
          ),
          pengumuman,
          lorem,
          CustomDividerView(),
          LiveForMaskView(),
        ],
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text('Customize'),
          backgroundColor: Colors.black87,
        ),
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(child: body)
    );
  }
}

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: const Center(
          child: MyStatefulWidget(),
        ),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  String dropdownSex = 'Choose Your Sex';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownSex,
      icon: const Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newSex) {
        setState(() {
          dropdownSex = newSex!;
        });
      },
      items: <String>['Choose Your Sex', 'Female', 'Male', 'Unisex']
          .map<DropdownMenuItem<String>>((String sex) {
        return DropdownMenuItem<String>(
          value: sex,
          child: Text(sex),
        );
      }).toList(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget2 extends StatefulWidget {
  const MyStatefulWidget2({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget2> createState() => _MyStatefulWidgetState2();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState2 extends State<MyStatefulWidget2> {
  String dropdownSize = 'Choose Your Size';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownSize,
      icon: const Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newSize) {
        setState(() {
          dropdownSize = newSize!;
        });
      },
      items: <String>['Choose Your Size', 'Extra Large', 'Large', 'Medium', 'Small']
          .map<DropdownMenuItem<String>>((String size) {
        return DropdownMenuItem<String>(
          value: size,
          child: Text(size),
        );
      }).toList(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget3 extends StatefulWidget {
  const MyStatefulWidget3({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget3> createState() => _MyStatefulWidget3();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidget3 extends State<MyStatefulWidget3> {
  String dropdownModel = 'Choose Your Model';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownModel,
      icon: const Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newModel) {
        setState(() {
          dropdownModel = newModel!;
        });
      },
      items: <String>['Choose Your Model','Extra Large', 'Large', 'Medium', 'Small']
          .map<DropdownMenuItem<String>>((String model) {
        return DropdownMenuItem<String>(
          value: model,
          child: Text(model),
        );
      }).toList(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget4 extends StatefulWidget {
  const MyStatefulWidget4({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget4> createState() => _MyStatefulWidget4();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidget4 extends State<MyStatefulWidget4> {
  String dropdownColor = 'Choose Your Color';

  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
      value: dropdownColor,
      icon: const Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String? newColor) {
        setState(() {
          dropdownColor = newColor!;
        });
      },
      items: <String>['Choose Your Color', 'Red', 'Green', 'Blue', 'Black']
          .map<DropdownMenuItem<String>>((String color) {
        return DropdownMenuItem<String>(
          value: color,
          child: Text(color),
        );
      }).toList(),
    );
  }
}

class LiveForMaskView extends StatelessWidget {
  const LiveForMaskView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20.0),
      padding: const EdgeInsets.all(0),
      height: 400.0,
      color: Colors.grey[200],
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "MASKER\nFOR\nLIFE's",
                style: Theme.of(context).textTheme.headline4!.copyWith(
                  color: Colors.grey[400],
                  fontSize: 80.0,
                  letterSpacing: 0.2,
                  height: 0.8,
                ),
              ),
              UIHelper.verticalSpaceLarge(),
              Text(
                'MADE BY RaFa',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.grey),
              ),
              Text(
                'C07, Pacil',
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(color: Colors.grey),
              ),
              UIHelper.verticalSpaceExtraLarge(),
              Row(
                children: <Widget>[
                  Container(
                    height: 1.0,
                    width: MediaQuery.of(context).size.width / 4,
                    color: Colors.grey,
                  ),
                ],
              )
            ],
          ),
          Positioned(
            left: 140.0,
            top: 90.0,
            child: Image.asset(
              'assets/images/masker.png',
              height: 80.0,
              width: 80.0,
            ),
          )
        ],
      ),
    );
  }
}



