import 'package:flutter/material.dart';
import 'package:pbp_c07/dummy_data.dart';
import 'package:pbp_c07/utils/app_colors.dart';
import 'package:pbp_c07/utils/ui_helper.dart';

class Keunggulan extends StatelessWidget {
  final keuntungan = DUMMY_KEUNTUNGAN;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      child: Column(
        children: <Widget>[
          LimitedBox(
            maxHeight: 188.0,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: keuntungan.length,
              itemBuilder: (context, index) => InkWell(
                  child: Container(
                    margin: const EdgeInsets.all(10.0),
                    height: 1000,
                    width: 350,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(8.0),
                                bottomRight: Radius.circular(8.0),
                              ),
                              child: Container(
                                width: 10.0,
                                height: 140.0,
                                color: c07Black,
                              ),
                            ),
                            UIHelper.horizontalSpaceMedium(),
                          Flexible(
                            child: Column(
                                children: <Widget>[

                                  Text(
                                    keuntungan[index].motto,
                                    textAlign: TextAlign.center,
                                    style: Theme.of(context).textTheme.headline4,
                                  ),
                                    UIHelper.verticalSpaceExtraSmall(),
                                    Text(
                                      keuntungan[index].penjelasan,
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                                        fontSize: 16.0,
                                        color: Colors.grey[800],
                                      ),
                                    )
                            ]
                          )
                        )
                          ]
                        )
                      ]
                    ),
                  ),
                ),
              ),
            ),
        ],
      )
    );
  }
}
