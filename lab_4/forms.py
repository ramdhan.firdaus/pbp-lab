from django.forms import ModelForm, TextInput, Textarea
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = '__all__'
        widgets = {
            'to_user' : TextInput(attrs={'class': 'form-control', 'placeholder':'Nama Penerima...'}),
            'from_user' : TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama Pengirim...'}),
            'title' : TextInput(attrs={'class': 'form-control', 'placeholder': 'Judul Pesan...'}),
            'message' : Textarea(attrs={'class': 'form-control', 'placeholder': 'Isi Pesan Yang Disampaikan...'})
        }