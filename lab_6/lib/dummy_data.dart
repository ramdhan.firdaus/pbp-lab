import 'package:pbp_c07/models/all_keunggulan.dart';
import 'package:pbp_c07/models/masker.dart';
import 'package:pbp_c07/models/top_picks_masker.dart';

const DUMMY_KEUNTUNGAN = const [
  AllKeuntungan(
    motto: 'Higienis',
    penjelasan: 'Kesterilan produk masker dijaga hingga ke tangan pembeli',
  ),
  AllKeuntungan(
    motto: 'Fashionable',
    penjelasan: 'Tidak hanya faktor kesehatan, kami selalu berusaha untuk menjaga faktor estetika produk masker kami',
  ),
  AllKeuntungan(
    motto: 'Terpercaya',
    penjelasan: 'Masker kami memiliki tingkat filtrasi hingga 70%',
  ),
];

const DUMMY_TOPMASKER = const [
  TopMasker(
      image: 'assets/images/masker1.jpg',
      name: 'Masker Bedah Biru',
      price: 'Rp2500'),
  TopMasker(
      image: 'assets/images/masker2.jpg',
      name: 'Masker Bedah Hitam',
      price: 'Rp3000'),
  TopMasker(
      image: 'assets/images/masker3.jpg',
      name: 'Masker Scuba',
      price: 'Rp3500'),
  TopMasker(
      image: 'assets/images/masker4.jpg',
      name: 'Masker Kain Polos',
      price: 'Rp4000'),
  TopMasker(
      image: 'assets/images/masker5.jpg',
      name: 'Masker Kain Vista',
      price: 'Rp5000'),
  TopMasker(
      image: 'assets/images/masker6.jpg',
      name: 'Masker N95',
      price: 'Rp6000'),
  TopMasker(
      image: 'assets/images/masker7.jpg',
      name: 'Masker KN95',
      price: 'Rp7000'),
  TopMasker(
      image: 'assets/images/masker8.jpg',
      name: 'Masker FFP3',
      price: 'Rp15000'),
  TopMasker(
      image: 'assets/images/masker9.jpg',
      name: 'Masker Rider',
      price: 'Rp25000'),
  TopMasker(
      image: 'assets/images/masker10.jpg',
      name: 'Masker N99',
      price: 'Rp28000'),
];

const DUMMY_MASKER = const [
  Masker(
    image: 'assets/images/masker1.jpg',
    name: 'Masker Bedah Biru',
    desc: 'Deskripsi Masker Bedah Biru',
    ratingPrice: '4.1 - Rp2500',
  ),
  Masker(
    image: 'assets/images/masker2.jpg',
    name: 'Masker Bedah Hitam',
    desc: 'Deskripsi Masker Bedah Hitam',
    ratingPrice: '4.3 - Rp3000',
  ),
  Masker(
    image: 'assets/images/masker3.jpg',
    name: 'Masker Scuba',
    desc: 'Deskripsi Masker Scuba',
    ratingPrice: '4.1 - Rp3500',
  ),
  Masker(
    image: 'assets/images/masker4.jpg',
    name: 'Masker Kain Polos',
    desc: 'Deskripsi Masker Kain Polos',
    ratingPrice: '4.3 - Rp4000',
  ),
  Masker(
    image: 'assets/images/masker5.jpg',
    name: 'Masker Kain Vista',
    desc: 'Deskripsi Masker Kain Vista',
    ratingPrice: '4.2 - Rp5000',
  ),
  Masker(
    image: 'assets/images/masker6.jpg',
    name: 'Masker N95',
    desc: 'Deskripsi Masker N95',
    ratingPrice: '3.8 - Rp6000',
  ),
  Masker(
    image: 'assets/images/masker7.jpg',
    name: 'Masker KN95',
    desc: 'Deskripsi Masker KN95',
    ratingPrice: '3.8 - Rp7000',
  ),
  Masker(
    image: 'assets/images/masker8.jpg',
    name: 'Masker FFP3',
    desc: 'Deskripsi Masker FFP3',
    ratingPrice: '3.8 - Rp15000',
  ),
  Masker(
    image: 'assets/images/masker9.jpg',
    name: 'Masker Rider',
    desc: 'Deskripsi Masker Rider',
    ratingPrice: '3.8 - Rp25000',
  ),
  Masker(
    image: 'assets/images/masker10.jpg',
    name: 'Masker N99',
    desc: 'Deskripsi Masker N99',
    ratingPrice: '3.8 - Rp28000',
  ),
];
